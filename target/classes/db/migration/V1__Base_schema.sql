CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE user_
(
  id         UUID                     DEFAULT uuid_generate_v4() NOT NULL
    CONSTRAINT user_pkey PRIMARY KEY,
  first_name TEXT                                                NOT NULL,
  last_name  TEXT                                                NOT NULL,
  email      TEXT                                                NOT NULL
    CONSTRAINT user_email_unique UNIQUE,
  created_at TIMESTAMP WITH TIME ZONE DEFAULT now()              NOT NULL,
  updated_at TIMESTAMP WITH TIME ZONE,
  deleted_at  TIMESTAMP WITH TIME ZONE
);
