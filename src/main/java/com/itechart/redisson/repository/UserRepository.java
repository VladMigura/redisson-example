package com.itechart.redisson.repository;

import com.itechart.redisson.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, UUID> {

    @Query(value = "SELECT * FROM user_ " +
            "WHERE deleted_at IS NULL " +
            "ORDER BY created_at DESC ",
            nativeQuery = true)
    List<UserEntity> findAll();

    @Query(value = "SELECT * FROM user_ " +
            "WHERE id = :id " +
            "AND deleted_at IS NULL ",
            nativeQuery = true)
    UserEntity findOneById(@Param("id") UUID id);

    @Modifying
    @Query(value = "UPDATE user_ " +
            "SET deleted_at = now() " +
            "WHERE id = :id " +
            "AND deleted_at IS NULL ",
            nativeQuery = true)
    void deleteOneById(@Param("id") UUID id);

}
