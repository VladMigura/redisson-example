package com.itechart.redisson.entity;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = false, of = "id")
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "user_")
public class UserEntity extends AbstractEntity {

    @Id
    @GeneratedValue
    @Column(name = "id", insertable = false, updatable = false, unique = true)
    private UUID id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email", unique = true)
    private String email;

}
