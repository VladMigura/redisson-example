package com.itechart.redisson.config;

import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.lang.String.format;

@Configuration
public class RedisConfig {

    @Bean
    public RedissonClient redissonClient(@Value("${spring.redis.host}") final String host,
                                         @Value("${spring.redis.port}") final String port) {
        final Config config = new Config();
        config.useSingleServer().setAddress(format("redis://%s:%s", host, port));
        config.setCodec(JsonJacksonCodec.INSTANCE);
        return Redisson.create(config);
    }

}
