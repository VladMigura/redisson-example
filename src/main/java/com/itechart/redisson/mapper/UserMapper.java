package com.itechart.redisson.mapper;

import com.itechart.redisson.entity.UserEntity;
import com.itechart.redisson.model.User;
import lombok.experimental.UtilityClass;

@UtilityClass
public class UserMapper {

    public static User toModel(final UserEntity userEntity) {
        return User.builder()
                .id(userEntity.getId())
                .firstName(userEntity.getFirstName())
                .lastName(userEntity.getLastName())
                .email(userEntity.getEmail())
                .createdAt(String.valueOf(userEntity.getCreatedAt()))
                .build();
    }

    public static UserEntity toEntity(final User user) {
        return UserEntity.builder()
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .email(user.getEmail())
                .build();
    }

}
