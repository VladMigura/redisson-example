package com.itechart.redisson.service.caching.impl;

import com.itechart.redisson.service.caching.CachingService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.concurrent.TimeUnit.SECONDS;

@Slf4j
@Service
@AllArgsConstructor
public class CachingServiceImpl implements CachingService {

    private final RedissonClient redissonClient;

    @Override
    public boolean containsEntry(final String key) {
        return redissonClient.getScoredSortedSet(key).remainTimeToLive() > 0;
    }

    @Override
    public <T> void save(final String key, final Integer ttl, final T object) {
        redissonClient.getBucket(key).set(object, ttl, SECONDS);
    }

    @Override
    public <T> void saveAll(final String key, final Integer ttl, final List<T> objects) {
        final RScoredSortedSet<Object> scoredSortedSet = redissonClient.getScoredSortedSet(key);
        scoredSortedSet.clear();
        for (int i = 0; i < objects.size(); i++) {
            scoredSortedSet.add(i, objects.get(i));
            scoredSortedSet.expire(ttl, SECONDS);
        }
    }

    @Override
    public <T> void add(final String key, final Integer ttl, final T object) {
        final RScoredSortedSet<Object> scoredSortedSet = redissonClient.getScoredSortedSet(key);
        scoredSortedSet.add(scoredSortedSet.size(), object);
        scoredSortedSet.expire(ttl, SECONDS);
    }
}
