package com.itechart.redisson.service.caching;

import java.util.List;

public interface CachingService {

    boolean containsEntry(String key);

    <T> void save(String key, Integer ttl, T object);

    <T> void saveAll(String key, Integer ttl, List<T> objects);

    <T> void add(String key, Integer ttl, T object);

}
