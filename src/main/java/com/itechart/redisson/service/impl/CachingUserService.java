package com.itechart.redisson.service.impl;

import com.itechart.redisson.model.User;
import com.itechart.redisson.service.UserService;
import com.itechart.redisson.service.caching.CachingService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Slf4j
@Service
public class CachingUserService implements UserService {

    private static final Integer WAIT_TIME_MILLIS = 1000;
    private static final String ALL_USERS_PREFIX = "all";

    private final RedissonClient redissonClient;
    private final CachingService cachingService;
    private final UserService userServiceImpl;

    private final String userKey;
    private final String userCompleteKey;
    private final String userLockKey;
    private final Integer userTtl;

    public CachingUserService(final RedissonClient redissonClient,
                              final CachingService cachingService,
                              final UserService userServiceImpl,
                              final @Value("${app.redis.user-key}") String userKey,
                              final @Value("${app.redis.user-complete-key}") String userCompleteKey,
                              final @Value("${app.redis.user-lock-key}") String userLockKey,
                              final @Value("${app.redis.user-ttl}") Integer userTtl) {
        this.redissonClient = redissonClient;
        this.cachingService = cachingService;
        this.userServiceImpl = userServiceImpl;

        this.userKey = userKey;
        this.userCompleteKey = userCompleteKey;
        this.userLockKey = userLockKey;
        this.userTtl = userTtl;
    }


    @Override
    public List<User> getAllUsers() {
        final String key = format(userKey, ALL_USERS_PREFIX);
        final String completeKey = format(userCompleteKey, ALL_USERS_PREFIX);
        if (!cachingService.containsEntry(key) || !cachingService.containsEntry(completeKey)) {
            populateAllUsersCache(key, completeKey);
        }
        return redissonClient.getScoredSortedSet(key)
                .stream()
                .map(User.class::cast)
                .collect(Collectors.toList());
    }

    @Override
    public User getUser(final UUID id) {
        final String key = format(userKey, id);
        final String completeKey = format(userCompleteKey, id);
        if (!cachingService.containsEntry(key) || !cachingService.containsEntry(completeKey)) {
            populateUserCache(id, key, completeKey);
        }
        return Optional.ofNullable(redissonClient.getBucket(key).get())
                .map(User.class::cast)
                .orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Override
    public User createUser(final User user) {
        final User created = userServiceImpl.createUser(user);
        final String key = format(userKey, ALL_USERS_PREFIX);
        final String completeKey = format(userCompleteKey, ALL_USERS_PREFIX);
        if (!cachingService.containsEntry(key) || !cachingService.containsEntry(completeKey)) {
            populateAllUsersCache(key, completeKey);
        } else {
            cachingService.add(key, userTtl, created);
        }
        return created;
    }

    @Override
    public void deleteUser(final UUID id) {
        userServiceImpl.deleteUser(id);
    }

    @SneakyThrows
    private void populateAllUsersCache(final String key, final String completeKey) {
        final List<User> users = userServiceImpl.getAllUsers();
        final RLock lock = redissonClient.getLock(format(userLockKey, ALL_USERS_PREFIX));
        try {
            lock.tryLock(WAIT_TIME_MILLIS, TimeUnit.MILLISECONDS);
            if (cachingService.containsEntry(key) && cachingService.containsEntry(completeKey)) {
                return;
            }
            cachingService.saveAll(key, userTtl, users);
            cachingService.save(completeKey, userTtl, ALL_USERS_PREFIX);
        } finally {
            lock.unlock();
        }
    }

    @SneakyThrows
    private void populateUserCache(final UUID id, final String key, final String completeKey) {
        final User user = userServiceImpl.getUser(id);
        final RLock lock = redissonClient.getLock(format(userLockKey, id));
        try {
            lock.tryLock(WAIT_TIME_MILLIS, TimeUnit.MILLISECONDS);
            if (cachingService.containsEntry(key) && cachingService.containsEntry(completeKey)) {
                return;
            }
            cachingService.save(key, userTtl, user);
            cachingService.save(completeKey, userTtl, String.valueOf(id));
        } finally {
            lock.unlock();
        }
    }

}
