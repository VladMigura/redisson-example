package com.itechart.redisson.service.impl;

import com.itechart.redisson.entity.UserEntity;
import com.itechart.redisson.mapper.UserMapper;
import com.itechart.redisson.model.User;
import com.itechart.redisson.repository.UserRepository;
import com.itechart.redisson.service.UserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.itechart.redisson.mapper.UserMapper.toEntity;
import static com.itechart.redisson.mapper.UserMapper.toModel;

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public List<User> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(UserMapper::toModel)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public User getUser(final UUID id) {
        return Optional.ofNullable(userRepository.findOneById(id))
                .map(UserMapper::toModel)
                .orElseThrow(() -> new RuntimeException("User not found"));
    }

    @Override
    @Transactional
    public User createUser(final User user) {
        final UserEntity entity = toEntity(user);
        final UserEntity saved = userRepository.saveAndFlush(entity);
        log.info("User saved successfully!");
        return toModel(saved);
    }

    @Override
    @Transactional
    public void deleteUser(final UUID id) {
        userRepository.deleteOneById(id);
        log.info("User with id {} deleted successfully");
    }
}
