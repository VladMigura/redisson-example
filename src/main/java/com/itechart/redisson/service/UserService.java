package com.itechart.redisson.service;

import com.itechart.redisson.model.User;

import java.util.List;
import java.util.UUID;

public interface UserService {

    List<User> getAllUsers();

    User getUser(UUID id);

    User createUser(User user);

    void deleteUser(UUID id);

}
