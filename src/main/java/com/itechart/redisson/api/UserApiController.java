package com.itechart.redisson.api;

import com.itechart.redisson.model.User;
import com.itechart.redisson.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@AllArgsConstructor
public class UserApiController {

    private static final String ALL_USERS_PATH = "/api/users";
    private static final String USER_PATH = "/api/users/{id}";

    private final UserService cachingUserService;

    @GetMapping(value = ALL_USERS_PATH, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<List<User>> getAllUsers() {
        final List<User> users = cachingUserService.getAllUsers();
        return new ResponseEntity<>(users, OK);
    }

    @GetMapping(value = USER_PATH, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUser(@PathVariable(name = "id") final UUID id) {
        final User user = cachingUserService.getUser(id);
        return new ResponseEntity<>(user, OK);
    }

    @PostMapping(value = ALL_USERS_PATH, consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<User> createUser(@RequestBody final User user) {
        final User saved = cachingUserService.createUser(user);
        return new ResponseEntity<>(saved, CREATED);
    }

    @DeleteMapping(value = USER_PATH)
    public ResponseEntity<Void> deleteUser(@PathVariable(name = "id") final UUID id) {
        cachingUserService.deleteUser(id);
        return new ResponseEntity<>(NO_CONTENT);
    }

}
